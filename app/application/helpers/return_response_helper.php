<?php

defined('BASEPATH') or exit('No direct script access allowed');

if (!function_exists('return_response')) {
    /**
     * 결과값 리턴
     *
     * @param int $httpCode
     * @param array $response
     * @return void
     */
    function return_response(int $httpCode, array $response)
    {
        $CI =  & get_instance();

        if (ENVIRONMENT != 'testing') {
            $CI->output
                ->set_content_type('application/json')
                ->set_status_header($httpCode)
                ->set_output(json_encode($response))
                ->_display();
            exit;
        } else {
            return $CI->output
                    ->set_content_type('application/json')
                    ->set_status_header($httpCode)
                    ->set_output(json_encode($response));
        }
    }
}
