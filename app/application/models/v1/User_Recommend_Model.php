<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class User_Recommend_Model
 */
class User_Recommend_Model extends My_Model
{
    /**
     * @var string $tableName 테이블 명
     */
    public $tableName = 'tbl_user_recommend';

    /**
     * 생성자
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
}
