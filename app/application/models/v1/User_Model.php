<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class User_Model
 */
class User_Model extends My_Model
{
    /**
     * @var string $tableName 테이블 명
     */
    public $tableName = 'tbl_user';

    /**
     * 생성자
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 유저 생성
     *
     * @param array $param
     * @param string $recommendNickName
     * @return bool
     */
    public function userCreate(array $param, string $recommendNickName) : bool
    {
        try {
            $this->db->trans_begin();

            $insert_ip = ip2long($param['insert_ip']);
            unset($param['insert_ip']);

            if (($userId = $this->insert($param)) === false) {
                throw new Exception();
            }

			$ci = & get_instance();

            if ($recommendNickName !== '') {

            	$recommendUserId = $this->select('id', [
                    ['where' => ['nickname', $recommendNickName]]
                ])[0]['id'];

            	if ($ci->user_recommend_model->count([
            	    ['where' => ['user_id', $userId]]
                ]) > 0) {
                    throw new Exception();
                }

            	$ci->user_recommend_model->insert([
                    'user_id'       => $userId,
                    're_user_id'    => $recommendUserId
                ]);
            }

			$ci->user_log_model->insert([
				'user_id' => $userId,
				'action'  => 'create',
                'insert_ip' => $insert_ip
			]);

            if ($this->db->trans_status() === false) {
                throw new Exception();
            }

            $this->db->trans_complete();

            $responseBool = true;
		} catch (MySQLDuplicateKeyException $e) {
			$this->db->trans_rollback();
			if (ENVIRONMENT === 'testing') {
				echo $e->getMessage();
			}
            $responseBool = false;
		} catch (MySQLException $e) {
			$this->db->trans_rollback();
			if (ENVIRONMENT === 'testing') {
				echo $e->getMessage();
			}
            $responseBool = false;
        } catch (Exception $e) {
            $this->db->trans_rollback();
            if (ENVIRONMENT === 'testing') {
            	echo $e->getMessage();
			}
            $responseBool = false;
        }

        return $responseBool;
    }

    /**
     * 유저 업데이트
     * 
     * @param string $checkSlug
     * @param array $param
     * @return bool
     */
    public function userUpdate(string $checkSlug, array $param) : bool
    {
        try {
            $this->db->trans_begin();

            $insert_ip = ip2long($param['insert_ip']);
            unset($param['insert_ip']);

            $this->update($param, [
                ['where' => ['slug', $checkSlug]],
                ['where' => ['delete_yn', 'N']]
            ]);

            $userId = $this->select('id', [
                ['where' => ['slug', $param['slug']]],
                ['where' => ['delete_yn', 'N']]
            ])[0]['id'];

            $ci = & get_instance();

            $ci->user_log_model->insert([
                'user_id' => $userId,
                'action'  => 'update',
                'insert_ip' => $insert_ip
            ]);

            if ($this->db->trans_status() === false) {
                throw new Exception();
            }

            $this->db->trans_complete();

            $responseBool = true;
        } catch (MySQLDuplicateKeyException $e) {
            $this->db->trans_rollback();
            if (ENVIRONMENT === 'testing') {
                echo $e->getMessage();
            }
            $responseBool = false;
        } catch (MySQLException $e) {
            $this->db->trans_rollback();
            if (ENVIRONMENT === 'testing') {
                echo $e->getMessage();
            }
            $responseBool = false;
        } catch (Exception $e) {
            $this->db->trans_rollback();
            if (ENVIRONMENT === 'testing') {
                echo $e->getMessage();
            }
            $responseBool = false;
        }
        return $responseBool;
    }

    /**
     * 유저 삭제
     *
     * @param array $param
     * @return bool
     */
    public function userDelete( array $param) : bool
    {
        try {
            $this->db->trans_begin();

            $this->update(['delete_yn' => 'Y'], [
                ['where' => ['slug', $param['slug']]]
            ]);

            $userId = $this->select('id', [
                ['where' => ['slug', $param['slug']]]
            ])[0]['id'];

            $ci = & get_instance();

            $ci->user_log_model->insert([
                'user_id' => $userId,
                'action'  => 'delete',
                'insert_ip' => ip2long($param['insert_ip'])
            ]);

            if ($this->db->trans_status() === false) {
                throw new Exception();
            }

            $this->db->trans_complete();

            $responseBool = true;
        } catch (MySQLDuplicateKeyException $e) {
            $this->db->trans_rollback();
            if (ENVIRONMENT === 'testing') {
                echo $e->getMessage();
            }
            $responseBool = false;
        } catch (MySQLException $e) {
            $this->db->trans_rollback();
            if (ENVIRONMENT === 'testing') {
                echo $e->getMessage();
            }
            $responseBool = false;
        } catch (Exception $e) {
            $this->db->trans_rollback();
            if (ENVIRONMENT === 'testing') {
                echo $e->getMessage();
            }
            $responseBool = false;
        }
        return $responseBool;
    }

    /**
     * 유저 리스트
     * 
     * @param array $param
     * @return mixed
     */
    public function userList(array $param)
    {
        return $this->db->query('
          SELECT 
            slug, nickname, NAME, hp, email
          FROM
            tbl_user
          WHERE 
            id > 0 AND delete_yn = "N"
          ORDER BY id DESC 
          LIMIT ?, ?  
        ',[ (int) $param['page'], (int) $param['limit']] )->result_array();
    }
}
