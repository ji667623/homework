<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class Month_send_coupon_sub
 */
class Month_send_coupon_sub extends My_Model
{
	/**
	 * @var string $tableName 테이블 명
	 */
	public $tableName = 'tbl_coupon_log';

	/**
	 * 생성자
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}
}
