<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class My_Model
 */
class My_Model extends CI_Model
{
    /**
     * 생성자
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

	/**
	 * 셀렉트
	 *
	 * @param string $selectString
	 * @param array $whereParam
	 * @param int $start
	 * @param int $limit
	 * @return array
	 */
	public function select(string $selectString = '*', array $whereParam = [], int $start = 0, int $limit = 0) : array
	{
		$this->db->select($selectString);
		$this->makeWhere($whereParam);

		if (($start + $limit) > 0) {
			$this->db->limit($start, $limit);
		}

		$this->db->order_by('id', 'desc');
		return $this->db->get($this->tableName)->result_array();
	}

    /**
     * 업데이트
     * 
     * @param array $param
     * @param array $whereParam
     * @return bool
     */
	public function update(array $param, array $whereParam = []) : bool
    {
        $this->makeWhere($whereParam);
        $this->db->update($this->tableName, $param);
        return true;
    }

    /**
     * 벌크 인설트
     * 
     * @param array $param
     * @return bool 
     */
    public function bulkInsert(array $param) : bool
    {
        return $this->db->insert_batch($this->tableName, $param);
    }

    /**
     * where 절
     *
     * @param array $param
     * @return void
     */
    public function makeWhere(array $param) : void
    {
        foreach ($param as  $val) {
            foreach ($val as $subKey => $subVal) {
                //var_dump($subKey);
                //var_dump(['asd', $subVal]);
                $this->db->{$subKey}($subVal[0], $subVal[1]);
            }
        }
    }

    /**
     * 생성
     *
     * @param array $param
     * @return int
     */
    public function insert(array $param) : int
    {
        $this->db->insert($this->tableName, $param);
        return $this->lastIndex();
    }

    /**
     * 테스트시 테이블 비우기
     * 
     * @return bool
     */
    public function truncate() : bool
    {
        if (ENVIRONMENT === 'testing') {
            $this->db->truncate($this->tableName);
        }
        return true;
    }

    /**
     * 카운트
     *
     * @param array $whereParam
     * @return int
     */
    public function count(array $whereParam = []) : int
    {
        $this->db->select('id');
        $this->db->from($this->tableName);

        if (sizeof($whereParam) > 0) {
            $this->makeWhere($whereParam);
        }

        return $this->db->count_all_results();
    }

	/**
	 * 마지막 인덱스
	 *
	 * @return int
	 */
    public function lastIndex()
	{
		return $this->db->insert_id();
	}
}
