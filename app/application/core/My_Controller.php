<?php
use Libraries\ResponseInfo;

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class My_Controller
 */
class My_Controller extends CI_Controller
{
    /**
     * 생성자
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    
        if (ENVIRONMENT !== 'testing' && $this->input->is_cli_request() === false) {
            $this->checkAtuh();
		}
    }

	/**
	 * 유닛 테스트가 아닐떄만 실행
	 *
	 * @return bool|void
	 */
    private function checkAtuh()
	{
		if ($_SERVER['AUTH_CHECK'] === false) {
			return return_response(
				ResponseInfo::$tokenError[0],
				ResponseInfo::$tokenError
			);
		}
		return true;
	}


    /**
     * 리스폰스 클래스 프로퍼티 확인
     *
     * @param string $errorPropertyName
     * @return bool
     * @throws ReflectionException
     */
    public function reflect(string $errorPropertyName) : bool
    {
        $reflection = new ReflectionClass(ResponseInfo::class);
        $properties = $reflection->getProperties(ReflectionProperty::IS_PUBLIC);
        $return = false;
        foreach ($properties as $property) {
            if ($errorPropertyName === $property->getName()) {
                $return = true;
                break;
            }
        }

        return $return;
    }

    /**
     * json body
     *
     * @return array
     */
    public function getJsonBodyToArray() : array
    {
        $requestArray = json_decode($this->input->raw_input_stream, true);
        return is_null($requestArray) ? [] : $requestArray;
    }

    /**
     * validation 체크
     * 
     * @param array $param
     * @return array
     */
    public function validateCheck(array $param = []) : array
    {
        $param = sizeof($param) > 0 ? $param : $this->getJsonBodyToArray();

        $this->form_validation->set_data($param);

        if ($this->form_validation->run() === false) {
			//var_dump(['a', $param]);
            return [];
        }
		//var_dump(['b', $param]);
        return $param;
    }

    /**
     * validation 리턴
     *
     * @param array $param
     * @return array
     */
    public function validateReturn(array $param = []) : array
    {
        if (sizeof($checkParam = $this->validateCheck($param)) > 0) {
            return ['success' => true, 'data' => $checkParam];
        }
        //var_dump($this->form_validation->error_array());
		//exit;
        $errorKeys  = array_keys($this->form_validation->error_array());

        if (sizeof($errorKeys) === 0) {
            $responseErrorArray = ResponseInfo::$serverError;
        }
        else {
            $responseErrorArray = ResponseInfo::$validate;
            $responseErrorArray[1] = str_replace('{KEY}', $errorKeys[0], $responseErrorArray[1]);
        }

        return ['success' => false, 'data' => $responseErrorArray];
    }
}
