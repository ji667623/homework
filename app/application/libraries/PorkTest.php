<?php 

class PorkTest
{   
    /**
     * @var object $ci
     */
    public $ci = null;

    /**
     * @var string $nowDate
     */ 
    public $nowDate = '';

    /**
     * @var array $jobArray
     */
    public $jobArray = [];

    /**
     * @var string $msg
     */
    public $msg = '';
    
    /**
     * @var array $bulkArray
     */
    private $bulkArray = [];

    /**
     * @var array $bulkLogArray
     */
    private $bulkLogArray = [];

    /**
     * 실행 
     * 
     * @return void
     */
    public function __invoke()
    {   
        foreach($this->jobArray as $key => $userId) {

            $coupon = base64_encode($userId);
            $created =  date('Y-m-d H:i:s');

            $this->bulkArray[] = [
                'user_id'  => $userId,
                'message'  => str_replace('{ID}', $userId, $this->msg ),
                'coupon'   => $coupon,
                'created'  => $created
            ];  

            $this->bulkLogArray[] = ['user_id' => $userId, 'coupon' => $coupon, 'created' => $created ];
        }
        
        $result = false;  
        if (count($this->bulkArray) > 0) {
            $result = $this->couponUserBatchInsert();
        }
        
        if ($result === count($this->bulkArray)) {
            $this->callNoticeService();
            $this->updateCouponCount();
        } 

        return true;
    }  
    
    /**
     * 파일 읽고 속성에 지정
     *  
     * @return void
     */
    public function readFileSetProperty($fullfilePath)
    {
        $string = read_file($fullfilePath);
        //var_dump($string);
        $this->jobArray = json_decode($string, true);
        //var_dump($this->jobArray);
        //exit;
    }

    /**
     * 푸시 서비스 호출
     * 
     * @return bool
     */
    private function callNoticeService()
    {   
        return $this->ci->month_send_coupon_sub->bulkInsert($this->bulkLogArray);
    }
    
    /**
     * 쿠폰 유저 등록
     * 
     * @return bool
     */
    private function couponUserBatchInsert() 
    {   
        return $this->ci->db->insert_batch('tbl_coupon_user', $this->bulkArray);    
    }

    /**
     * 쿠폰 카운트 업데이트
     *
     * @return void 
     */ 
    private function updateCouponCount()
    {
        $this->ci->db->set('coupon_cnt', 'coupon_cnt + '.count($this->bulkLogArray), false)
            ->where('id', 1)->update('tbl_coupon_count');    
        //var_dump($this->ci->db->last_query());
    }
}