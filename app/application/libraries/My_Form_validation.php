<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');


class My_Form_validation extends CI_Form_validation
{
    /**
     * @var CI_Controller|object
     */
    protected  $CI;

    /**
     * My_Form_validation constructor.
     * @param array $rules
     */
    public function __construct(array $rules = [])
    {
        parent::__construct($rules);

        $this->CI = & get_instance();
    }

    /**
     * 유저 이름 체크
     *
     * @param string $name
     * @return bool
     */
    public function userNameCheck(string $name) : bool
    {
        preg_match_all('/([\xEA-\xED][\x80-\xBF]{2}|[a-zA-Z])+/', trim($name), $check);
        if ($name !== implode("",$check[0])) {
            return false;
        }

        $check = preg_replace("/\s+/", "", $name);
        if ($name !== $check) {
            return false;
        }

        return true;
    }

    /**
     * 유저 닉네임 체크
     *
     * @param string $nickname
     * @return bool
     */
    public function userNicknameCheck(string $nickname) : bool
    {
        if (strtolower(trim($nickname)) !== $nickname) {
            return false;
        }

        $check = preg_replace("/\s+/", "", $nickname);
        if ($nickname !== $check) {
            return false;
        }

        return true;
    }

    /**
     * 패스워드 체크
     *
     * @param string $password
     * @return bool
     */
    public function userPasswordCheck(string $password) : bool
    {
        $check = true;

        $checkArray = [
            '/[A-Z]/', '/[a-z]/', '/[0-9]/', '/[!#$%^&*()?+=\/]/'
        ];

        foreach ($checkArray as $pattern) {
            preg_match_all($pattern, $password, $match);
            if (sizeof($match[0]) === 0) {
                //var_dump('실패 '.$pattern.' : '.$password);
                $check = false;
                break;
            }
        }

        if ($check === false) {
            return false;
        }

        $check = preg_replace("/\s+/", "", $password);
        if ($password !== $check) {
            return false;
        }
        return true;
    }

    /**
     * 추천인 체크
     *
     * @param string $recommendNickName
     * @return bool
     */
    public function recommendCheck(string $recommendNickName = null)
    {
        if ($recommendNickName === null) {
            return false;
        }

        $whereArray = [
            ['where' => ['nickname', $recommendNickName]],
            ['where' => ['delete_yn', 'N']]
        ];

        $userCount = $this->CI->user_model->count($whereArray);
        //echo $this->CI->db->last_query();
        //var_dump([123,$userCount]);exit;
        if ($userCount === 0) {
            return false;
        }

        $recommendUserId = $this->CI->user_model->select('id', $whereArray)[0]['id'];

        $recommendCount = $this->CI->user_recommend_model->count([
            ['where' => ['re_user_id', $recommendUserId]]
        ]);

        if ($recommendCount >= 5) {
            return false;
        }
        return true;
    }

    /**
     * 유저 SLUG 카운트
     * 
     * @param string $slug
     * @return bool
     */
    public function countUserSlug(string $slug) : bool
    {
        $count = $this->CI->user_model->count([
            ['where' => ['slug', $slug]],
            ['where' => ['delete_yn', 'N']]
        ]);

        return $count === 0 ? false : true;
    }

    /**
     * 유저 수정 시 자기를 제외한 유니크 체크
     * 
     * @param array $param
     * @return array
     */
    public function checkUpdateUnique(array $param) : array
    {
        $checkArray = ['nickname', 'hp', 'email'];
        $returnArray = ['success' => true];
        foreach ($checkArray as $check) {
            $count = $this->CI->user_model->count([
                ['where' => ['slug <> ', $param['slug']]],
                ['where' => [$check, $param[$check]]]
            ]);

            if ($count > 0) {
                $returnArray['success'] = false;
                $returnArray['field']  = $check;
                break;
            }
        }
        return $returnArray;
    }

    /**
     * 패스워드 체크
     * 
     * @param array $param
     * @return bool
     */
    public function checkPassword(array $param) : bool
    {
        $dbPassword = $this->CI->user_model->select('password', [
            ['where' => ['slug ', $param['slug']]],
        ])[0]['password'];

        return password_verify($param['password'], $dbPassword);
    }
}
