<?php
namespace Libraries;

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class ResponseInfo
 * @package Libraries
 */
class ResponseInfo
{
    /**
     * @var array 벨리데이션 실패 코드, 메시지
     */
    public static $validate = [400, '{KEY}를 확인 해 주세요.'];

    /**
     * @var array 리스폰스 없음 코드, 메시지
     */
    public static $emptyResponse = [404, '존재하지 않거나 데이터가 없습니다.'];

    /**
     * @var array 잘못 된 경로 코드, 메시지
     */
    public static $wrongUri = [404, '잘못 된 경로 입니다.'];

    /**
     * @var array 서버 사이드 에러
     */
    public static $serverError = [500, '관리자에게 문의 해 주세요.'];

    /**
     * @var array 토큰 에러 코드, 메시지
     */
    public static $tokenError = [401, '잘못 된 토큰 입니다.'];

    /**
     * @var array 토큰 만료 코드, 메시지
     */
    public static $tokenExpire = [400, '토큰이 만료 돼 었습니다.'];
}