<?php

/**
 * name
 * 공백제거|xss_clean|정수
 *
 * nickname
 * 공백제거|xss_clean|정수
 *
 * */
$config['user/list'] = [
    [
        'field' => 'page',
        'label' => 'page',
        'rules' => 'trim|xss_clean|integer|is_natural_no_zero'
    ],
    [
        'field' => 'limit',
        'label' => 'limit',
        'rules' => 'trim|xss_clean|integer|is_natural_no_zero'
    ]
];
