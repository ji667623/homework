<?php
/**
 * slug
 * 공백제거|필수|xss_clean|custom
 *
 * password
 * 공백제거|필수|xss_clean|최소길이10|최대길이255|커스텀
 *
 * */

$config['user/delete'] = [
    [
        'field' => 'slug',
        'label' => 'slug',
        'rules' => 'trim|required|xss_clean|countUserSlug'
    ],
    [
        'field' => 'password',
        'label' => 'password',
        'rules' => 'trim|required|xss_clean|min_length[10]|max_length[255]|userPasswordCheck'
    ]
];