<?php

/**
 * name
 * 공백제거|필수|xss_clean|최소길이2|최대길이20|custom
 *
 * nickname
 * 공백제거|필수|xss_clean|최소길이1|최대길이30|custom|영문|유니크
 *
 * password
 * 공백제거|필수|xss_clean|최소길이10|최대길이255|커스텀
 *
 * password_confirm
 * 필수|password매칭
 *
 * hp
 * 공백제거|필수|xss_clean|숫자
 *
 * email
 * 공백제거|필수|xss_clean|이메일|최대길이100
 *
 * sex
 * 공백제거|xss_clean|포함[none,male,female]
 *
 * 추천인 닉네임
 * 공백제거|xss_clean|커스텀
 * */
$config['user/create'] = [
    [
        'field' => 'name',
        'label' => 'name',
        'rules' => 'trim|required|xss_clean|min_length[2]|max_length[20]|userNameCheck'
    ],
    [
        'field' => 'nickname',
        'label' => 'nickname',
        'rules' => 'trim|required|xss_clean|max_length[30]|userNicknameCheck|alpha|is_unique[tbl_user.nickname]'
    ],
    [
        'field' => 'password',
        'label' => 'password',
        'rules' => 'trim|required|xss_clean|min_length[10]|max_length[255]|userPasswordCheck'
    ],
    [
        'field' => 'password_confirm',
        'label' => 'password_confirm',
        'rules' => 'required|matches[password]'
    ],
    [
        'field' => 'hp',
        'label' => 'hp',
        'rules' => 'trim|required|xss_clean|numeric|max_length[20]|is_unique[tbl_user.hp]'
    ],
    [
        'field' => 'email',
        'label' => 'email',
        'rules' => 'trim|required|xss_clean|valid_email|max_length[100]|is_unique[tbl_user.email]'
    ],
    [
        'field' => 'sex',
        'label' => 'sex',
        'rules' => 'trim|xss_clean|in_list[none,male,female]'
    ],
    [
        'field' => 'recommend_nickname',
        'label' => 'recommend_nickname',
        'rules' => 'trim|xss_clean|recommendCheck'
    ]
];
