<?php
/**
 * slug
 * 공백제거|필수|xss_clean|custom
 *
 * */

$config['user/info'] = [
    [
        'field' => 'slug',
        'label' => 'slug',
        'rules' => 'trim|required|xss_clean|countUserSlug'
    ]
];