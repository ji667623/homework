<?php

defined('BASEPATH') or exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| Hooks
| -------------------------------------------------------------------------
| This file lets you define "hooks" to extend CI without hacking the core
| files.  Please see the user guide for info:
|
|	https://codeigniter.com/user_guide/general/hooks.html
|
*/
$hook['pre_system'][] = [
    'class'     => 'My_Init',
    'function'  => 'initExec',
    'filename'  => 'My_Init.php',
    'filepath'  => 'hooks'
];

$hook['pre_controller'][] = [
    'class' => 'My_Auth',
    'function' => 'checkAuthorizationKey',
    'filename' => 'My_Auth.php',
    'filepath' => 'hooks'
];
