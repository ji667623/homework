<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$validationPath = APPPATH.'config/validation';
$includeArray = [
    'v1/user/create.php', 'v1/user/update.php', 'v1/user/delete.php',
    'v1/user/info.php', 'v1/user/list.php'
];

$config = [];

foreach ($includeArray as $fileName) {
    include  $validationPath.'/'.$fileName;
}
