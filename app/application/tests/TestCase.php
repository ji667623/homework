<?php
use Faker\Factory;
use Libraries\ResponseInfo;

/**
 * Class TestCase
 */
class TestCase extends CIPHPUnitTestCase
{
    /**
     * @var  Faker\Factory $faker
     */
    public $faker;

    /**
     * @var string
     */
    public $validateErrorMsg = '';

    /**
     * @return void
     */
    public function setUp()
    {
        $this->resetInstance();
        $this->CI->load->database();

        $this->faker = Factory::create('ko_KR');
        $this->validateErrorMsg = ResponseInfo::$validate[1];
    }

    /**
     * 패스워드 생성
     *
     * @return string
     */
    public function makePassword() : string
    {
        return $this->faker->password.'aA1*';
    }

    /**
     * request mock 후 리턴
     * @param $obj
     * @param array $param
     * @param array $methods
     * @return bool
     */
    public function returnRequestsetCallable(& $obj, array $param = [], array $methods = []) : bool
    {
        $obj->request->setCallable(function ($CI) use ($param, $methods) {
            if (sizeof($param) > 0) {
                ReflectionHelper::setPrivateProperty(
                    $CI->input,
                    '_raw_input_stream',
                    json_encode($param)
                );
            }
            foreach ($methods as $key => $val) {
                $CI->{$key} = $this->getDouble($val['namespace'], $val['methods']);
            }
        });
        return true;
    }

    /**
     * 유저 등록
     *
     * @param array $param
     * @return string
     */
    public function userInsert(array $param) : string
    {
        unset($param['password_confirm']);
        $param['slug'] = hash('sha1', $param['email'].$param['hp']);
        $param['password'] = password_hash($param['password'], PASSWORD_DEFAULT);
        $this->CI->user_model->insert($param);
        return $param['slug'];
    }

    /**
     * 유저 필수 파라메터 정보 생성
     *
     * @return array
     */
    public function returnUserCreateParam() : array
    {
        //$password = $this->makePassword();
        $password = '!!@@12asBFCFze%';
        return   [
            'name' =>  $this->faker->name,
            'nickname'  => $this->faker->firstNameAscii(),
            'password'  => $password,
            'password_confirm' => $password,
            'hp'               => str_replace('-', '', $this->faker->PhoneNumber),
            'email'            => $this->faker->email
        ];
    }
}
