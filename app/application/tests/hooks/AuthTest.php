<?php

require_once APPPATH.'/hooks/My_Auth.php';

class AuthTest  extends TestCase
{
    public $auth;

    /**
     * 셋업
     *
     * @return void
     */
    public function setUp()
    {
        $this->auth = new My_Auth();
    }

    /**
     * @test API KEY CHECK
     *
     * @return void
     */
    public function test_auth_key()  : void
    {
        $this->auth->checkAuthorizationKey('bearer 4124124124124');

        $this->assertEquals($_SERVER['AUTH_CHECK'], false);

        $this->auth->checkAuthorizationKey('bearer '.API_KEY);

        $this->assertEquals($_SERVER['AUTH_CHECK'], true);
    }
}