<?php
use Libraries\ResponseInfo;

/**
 * Part of ci-phpunit-test
 *
 * @author     Kenji Suzuki <https://github.com/kenjis>
 * @license    MIT License
 * @copyright  2015 Kenji Suzuki
 * @link       https://github.com/kenjis/ci-phpunit-test
 */

class WelcomeTest extends TestCase
{
    /**
     * @test 인덱스
     *
     * @return void
     */
    public function test_index()
    {
        $output = $this->request('GET', 'welcome/index');
        $this->assertResponseCode(404);
        $this->assertEquals(json_encode(ResponseInfo::$wrongUri), $output);
    }

    /**
     * @test 잘못된 경로로 가면 4040가 뜬다.
     *
     * @return void
     */
    public function test_method_404()
    {
        $this->request('GET', 'welcome/method_not_exist');
        $this->assertResponseCode(404);
    }

    /**
     * @test 앱 경로가 동일하다
     *
     * @return void
     */
    public function test_APPPATH()
    {
        $actual = realpath(APPPATH);
        $expected = realpath(__DIR__ . '/../..');
        $this->assertEquals(
            $expected,
            $actual,
            'Your APPPATH seems to be wrong. Check your $application_folder in tests/Bootstrap.php'
        );
    }
}
