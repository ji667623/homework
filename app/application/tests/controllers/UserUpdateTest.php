<?php

/**
 * Class UserUpdateTest
 */
class UserUpdateTest extends TestCase
{
    /**
     * 유닛 테스트 실행시 자동 실행
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->CI->load->model('v1/user_model', 'user_model');
        $this->CI->load->model('v1/user_log_model', 'user_log_model');

        $this->CI->user_model->truncate();
        $this->CI->user_log_model->truncate();
    }

    /**
     * @test slug,
     *
     * 공백제거|필수|xss_clean|최소길이2|최대길이20|custom
     *
     * @return void
     */
    public function test_user_update_slug()  : void
    {
        $param = $this->returnUserUpdateParam();
        unset($param['slug']);
        $checkSlugArray = ['', '65756sgfhs56874584586'];

        $errorMsg = str_replace('{KEY}', 'slug', $this->validateErrorMsg);

        foreach ($checkSlugArray as $slug) {
            $this->returnRequestsetCallable($this, $param);
            $output = $this->request('PUT', 'api/user/'.$slug);

            if ($slug === '') {
                $this->assertResponseCode(404);
            } else {
                //var_dump($output);
                $this->assertResponseCode(400);

                $outputDecode = json_decode($output, true);
                //var_dump($outputDecode);
                $this->assertEquals($outputDecode[1], $errorMsg);
            }
            unset($output);
        }
    }


    /**
     * @test 유저명 한글,
     *
     * 공백제거|필수|xss_clean|최소길이2|최대길이20|custom
     *
     * @return void
     */
    public function test_user_update_name() : void
    {
        $param = $this->returnUserUpdateParam();

        $slug = $param['slug'];
        unset($param['slug']);

        $checkNameArray = ['123a', '!as!!dasd', 'F한글ASDSASDASsASDASDSA'];

        $errorMsg = str_replace('{KEY}', 'name', $this->validateErrorMsg);

        foreach ($checkNameArray as $checkName) {
            $param['name'] = $checkName;

            $this->returnRequestsetCallable($this, $param);

            $output = $this->request('PUT', 'api/user/'.$slug);
            //var_dump($output);
            $this->assertResponseCode(400);

            $outputDecode = json_decode($output, true);
            //var_dump($outputDecode);
            $this->assertEquals($outputDecode[1], $errorMsg);
            unset($output);
        }
    }

    /**
     * @test 닉네임,
     *
     * 영문 대소문자만 허용  공백제거|필수|xss_clean|최소길이1|최대길이30|custom|영문
     *
     * @return void
     */
    public function test_user_update_nickname() : void
    {
        $insertParam = $this->returnUserCreateParam();
        $this->userInsert($insertParam);

        $param = $this->returnUserUpdateParam();

        $slug = $param['slug'];
        unset($param['slug']);

        $errorMsg = str_replace('{KEY}', 'nickname', $this->validateErrorMsg);

        $checkNickNameArray = [
            'aaa1', 'asdDsd', 'asda한글sdasd', 'dgs!@#/*~!2ads',
            'asdfghjklzxcvbnmqwertyuioplkjhg', $insertParam['nickname']
        ];

        foreach ($checkNickNameArray as $nickName) {
            $param['nickname'] = $nickName;

            $this->returnRequestsetCallable($this, $param);

            $output = $this->request('PUT', 'api/user/'.$slug);
            //var_dump($output);
            $this->assertResponseCode(400);

            $outputDecode = json_decode($output, true);
            //var_dump($output);
            $this->assertEquals($outputDecode[1], $errorMsg);
            unset($output);
        }
    }

    /**
     * @test 핸드폰
     *
     * 공백제거|필수|xss|숫자|최대20
     * @return void
     */
    public function test_user_update_hp() : void
    {
        $insertParam = $this->returnUserCreateParam();
        $this->userInsert($insertParam);

        $param = $this->returnUserUpdateParam();

        $slug = $param['slug'];
        unset($param['slug']);

        $checkHpArray = [
            '010-2851-6676', '01a141812', '121212312312312451234', '01f028516676', $insertParam['hp']
        ];

        $errorMsg = str_replace('{KEY}', 'hp', $this->validateErrorMsg);

        foreach ($checkHpArray as $hp) {
            $param['hp'] = $hp;
            //var_dump($param);
            $this->returnRequestsetCallable($this, $param);

            $output = $this->request('PUT', 'api/user/'.$slug);
            //var_dump($output);
            $this->assertResponseCode(400);

            $outputDecode = json_decode($output, true);

            $this->assertEquals($outputDecode[1], $errorMsg);
            unset($output);
        }
    }

    /**
     * @test 이메일
     *
     * 공백제거|필수|xss_clean|이메일|최대길이100|custom
     * @return void
     */
    public function test_user_update_email() : void
    {
        $insertParam = $this->returnUserCreateParam();
        $this->userInsert($insertParam);

        $param = $this->returnUserUpdateParam();

        $slug = $param['slug'];
        unset($param['slug']);

        $checkEmailArray = [
            'asdas346gfdsdfg', 'fgdas56546', 'asfgagdgsd56', '436346346', $insertParam['email']
        ];

        $errorMsg = str_replace('{KEY}', 'email', $this->validateErrorMsg);

        foreach ($checkEmailArray as $email) {
            $param['email'] = $email;
            //var_dump($param);
            $this->returnRequestsetCallable($this, $param);

            $output = $this->request('PUT', 'api/user/'.$slug);
            //var_dump($output);
            $this->assertResponseCode(400);

            $outputDecode = json_decode($output, true);

            $this->assertEquals($outputDecode[1], $errorMsg);
            unset($output);
        }
    }

    /**
     * @test 성별
     *
     * 공백제거|xss_clean|포함[none,male,female]
     * @return void
     */
    public function test_user_update_sex() : void
    {
        $param = $this->returnUserUpdateParam();

        $slug = $param['slug'];
        $param['sex'] = 'asd';
        unset($param['slug']);

        $errorMsg = str_replace('{KEY}', 'sex', $this->validateErrorMsg);
        //var_dump($param);
        $this->returnRequestsetCallable($this, $param);

        $output = $this->request('PUT', 'api/user/'.$slug);
        //var_dump($output);
        $this->assertResponseCode(400);

        $outputDecode = json_decode($output, true);

        $this->assertEquals($outputDecode[1], $errorMsg);
        unset($output);
    }

    /**
     * @test 등록
     *
     * @return void
     */
    public function test_user_update_success() : void
    {
        $param = $this->returnUserUpdateParam();

        $slug = $param['slug'];
        unset($param);

        $param = $this->returnUserCreateParam();

        $param['sex'] = 'female';
        unset($param['password_confirm']);

        $this->returnRequestsetCallable($this, $param);

        $output = $this->request('PUT', 'api/user/'.$slug);

        $this->assertResponseCode(200);

        $outputDecode = json_decode($output, true);

        $param['slug'] = hash('sha1', $param['email'].$param['hp']);

        $this->assertEquals($outputDecode, $param);
        unset($output);

        $userId = $this->CI->user_model->select('id', [
            ['where' => ['slug', $param['slug']]],
            ['where' => ['delete_yn', 'N']]
        ])[0]['id'];

        $this->assertEquals(1, $this->CI->user_log_model->count([
            ['where' => ['user_id', $userId]],
            ['where' => ['action', 'update']]
        ]));
    }

    /**
     * 유저 필수 파라메터 정보 생성
     *
     * @return array
     */
    public function returnUserUpdateParam() : array
    {
        $param =  $insertParam =  $this->returnUserCreateParam();

        $insertParam['slug'] = $param['slug'] = hash('sha1', $insertParam['email'].$insertParam['hp']);
        $insertParam['password'] = password_hash($insertParam['password'], PASSWORD_DEFAULT);
        unset($param['password']);
        unset($param['password_confirm']);
        unset($insertParam['password_confirm']);

        $this->CI->user_model->insert($insertParam);
        unset($insertParam);

        return $param;
    }
}
