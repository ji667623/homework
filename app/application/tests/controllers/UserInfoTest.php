<?php

/**
 * Class UserInfoTest
 */
class UserInfoTest extends TestCase
{
    /**
     * 유닛 테스트 실행시 자동 실행
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->CI->load->model('v1/user_model', 'user_model');

        $this->CI->user_model->truncate();
    }

    /**
     * @test slug,
     *
     * 공백제거|필수|xss_clean|최소길이2|최대길이20|custom
     *
     * @return void
     */
    public function test_user_info_slug()  : void
    {
        $param = $this->returnUserCreateParam();
        $param['delete_yn'] = 'Y';

        $slug = $this->userInsert($param);
        unset($param);

        $checkSlugArray = ['', '65756sgfhs56874584586', $slug];

        $errorMsg = str_replace('{KEY}', 'slug', $this->validateErrorMsg);

        foreach ($checkSlugArray as $slug) {
            $output = $this->request('GET', 'api/user/info/'.$slug);
            //var_dump($output);
            if ($slug === '') {
                $this->assertResponseCode(404);
            } else {
                $this->assertResponseCode(400);

                $outputDecode = json_decode($output, true);
                //var_dump($outputDecode);
                $this->assertEquals($outputDecode[1], $errorMsg);
            }
            unset($output);
        }
    }

    /**
     * @test success,
     *
     * 공백제거|필수|xss_clean|최소길이2|최대길이20|custom
     *
     * @return void
     */
    public function test_user_info_success()  : void
    {
        $param = $this->returnUserCreateParam();

        $slug = $this->userInsert($param);

        $output = $this->request('GET', 'api/user/info/'.$slug);

        $this->assertResponseCode(200);

        $outputDecode = json_decode($output, true);
        //var_dump($outputDecode);
        $this->assertEquals($outputDecode, [
            'slug' => $slug, 'name' => $param['name'],
            'nickname' => $param['nickname'], 'hp' => $param['hp'],
            'email' => $param['email']
        ]);
        unset($output);
    }
}