<?php

/**
 * Class UserDeleteTest
 */
class UserDeleteTest extends TestCase
{
    /**
     * 유닛 테스트 실행시 자동 실행
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->CI->load->model('v1/user_model', 'user_model');
        $this->CI->load->model('v1/user_log_model', 'user_log_model');

        $this->CI->user_model->truncate();
        $this->CI->user_log_model->truncate();
    }

    /**
     * @test slug,
     *
     * 공백제거|필수|xss_clean|최소길이2|최대길이20|custom
     *
     * @return void
     */
    public function test_user_delete_slug()  : void
    {
        $param = $this->returnUserCreateParam();
        $param['delete_yn'] = 'Y';

        $slug = $this->userInsert($param);
        $password = $param['password'];
        unset($param);

        $param = [
            'password' => $password
        ];

        $checkSlugArray = ['', '65756sgfhs56874584586', $slug];

        $errorMsg = str_replace('{KEY}', 'slug', $this->validateErrorMsg);

        foreach ($checkSlugArray as $slug) {

            $this->returnRequestsetCallable($this, $param);

            $output = $this->request('DELETE', 'api/user/'.$slug);
            //var_dump($output);
            if ($slug === '') {
                $this->assertResponseCode(404);
            } else {
                $this->assertResponseCode(400);

                $outputDecode = json_decode($output, true);
                //var_dump($outputDecode);
                $this->assertEquals($outputDecode[1], $errorMsg);
            }
            unset($output);
        }
    }

    /**
     * @test password,
     *
     * 'trim|required|xss_clean|min_length[10]|max_length[255]|userPasswordCheck'
     *
     * @return void
     */
    public function test_user_delete_password() : void
    {
        $param = $this->returnUserCreateParam();

        $slug = $this->userInsert($param);
        unset($param);

        $checkPasswordArray = [
            '123456asdD', '!@#$%asdf2', '!@#$%ASDF7', '0M=!|>zD3g'
        ];

        $errorMsg = str_replace('{KEY}', 'password', $this->validateErrorMsg);

        $param = [];

        foreach ($checkPasswordArray as $password) {
            $param['password'] = $password;
            $this->returnRequestsetCallable($this, $param);

            $output = $this->request('DELETE', 'api/user/'.$slug);
            //var_dump($output);
            if ($slug === '') {
                $this->assertResponseCode(404);
            } else {
                $this->assertResponseCode(400);

                $outputDecode = json_decode($output, true);
                //var_dump($outputDecode);
                $this->assertEquals($outputDecode[1], $errorMsg);
            }
            unset($output);
        }
    }

    /**
     * @test success,
     *
     *
     * @return void
     */
    public function test_user_delete_success()  : void
    {
        $param = $this->returnUserCreateParam();

        $slug = $this->userInsert($param);

        $this->returnRequestsetCallable($this, ['password' => $param['password']]);
        unset($param);

        $output = $this->request('DELETE', 'api/user/'.$slug);
        //var_dump($output);
        $this->assertResponseCode(200);

        $outputDecode = json_decode($output, true);
        //var_dump($outputDecode);
        $this->assertEquals($outputDecode, ['slug' => $slug]);
    }
}
