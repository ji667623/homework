<?php

/**
 * Class UserCreateTest
 */
class UserCreateTest extends TestCase
{
    /**
     * 유닛 테스트 실행시 자동 실행
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->CI->load->model('v1/user_model', 'user_model');
        $this->CI->load->model('v1/user_recommend_model', 'user_recommend_model');
        $this->CI->load->model('v1/user_log_model', 'user_log_model');

        $this->CI->user_model->truncate();
        $this->CI->user_recommend_model->truncate();
        $this->CI->user_log_model->truncate();
    }

    /**
     * @test 유저명 한글,
     *
     * 공백제거|필수|xss_clean|최소길이2|최대길이20|custom
     *
     * @return void
     */
    public function test_user_name() : void
    {
        $param = $this->returnUserCreateParam();

        $checkNameArray = ['123a', '!as!!dasd', 'F한글ASDSASDASsASDASDSA'];

        $errorMsg = str_replace('{KEY}', 'name', $this->validateErrorMsg);

        foreach ($checkNameArray as $checkName) {
            $param['name'] = $checkName;

            $this->returnRequestsetCallable($this, $param);

            $output = $this->request('POST', 'api/user');
            //var_dump($output);
            $this->assertResponseCode(400);

            $outputDecode = json_decode($output, true);
            //var_dump($outputDecode);
            $this->assertEquals($outputDecode[1], $errorMsg);
            unset($output);
        }
    }

    /**
     * @test 닉네임,
     *
     * 영문 대소문자만 허용  공백제거|필수|xss_clean|최소길이1|최대길이30|custom|영문|유니크
     *
     * @return void
     */
    public function test_user_nickname() : void
    {
        $param = $this->returnUserCreateParam();

        $this->userInsert($param);

        $errorMsg = str_replace('{KEY}', 'nickname', $this->validateErrorMsg);

        $checkNickNameArray = [
            'aaa1', 'asdDsd', 'asda한글sdasd', 'dgs!@#/*~!2ads',
            'asdfghjklzxcvbnmqwertyuioplkjhg'
        ];

        foreach ($checkNickNameArray as $nickName) {
            $param['nickname'] = $nickName;
            //var_dump($param);
            $this->returnRequestsetCallable($this, $param);

            $output = $this->request('POST', 'api/user');
            //var_dump($output);
            $this->assertResponseCode(400);

            $outputDecode = json_decode($output, true);
            //var_dump($output);
            $this->assertEquals($outputDecode[1], $errorMsg);
            unset($output);
        }
    }

    /**
     * @test 패스워드
     *
     *  최소 1개의 대문자 소문자 특수문자 숫자 포함 공백제거|필수|xss_clean|최소길이10|최대길이255|커스텀
     *
     * @return void
     */
    public function test_user_password() : void
    {
        $param = $this->returnUserCreateParam();

        $checkPasswordArray = [
            '123456asdD', '!@#$%asdf2', '!@#$%ASDF7', '0M=!|>zD'
        ];

        $errorMsg = str_replace('{KEY}', 'password', $this->validateErrorMsg);

        foreach ($checkPasswordArray as $password) {
            $param['password'] = $password;
            //var_dump($param);
            $this->returnRequestsetCallable($this, $param);

            $output = $this->request('POST', 'api/user');
            //var_dump($output);
            $this->assertResponseCode(400);

            $outputDecode = json_decode($output, true);

            $this->assertEquals($outputDecode[1], $errorMsg);
            unset($output);
        }
    }

    /**
     * @test 패스워드 확인
     *
     * 필수|password매칭
     *
     * @return void
     */
    public function test_user_password_confirm() : void
    {
        $param = $this->returnUserCreateParam();

        $param['password_confirm'] = '124124';

        $errorMsg = str_replace('{KEY}', 'password_confirm', $this->validateErrorMsg);

        //var_dump($param);
        $this->returnRequestsetCallable($this, $param);
        $output = $this->request('POST', 'api/user');
        //var_dump($output);
        $this->assertResponseCode(400);

        $outputDecode = json_decode($output, true);

        $this->assertEquals($outputDecode[1], $errorMsg);
        unset($output);
    }

    /**
     * @test 핸드폰
     *
     * 공백제거|필수|xss|숫자|최대20|유니크
     * @return void
     */
    public function test_user_hp() : void
    {
        $param = $this->returnUserCreateParam();

        $this->userInsert($param);

        $param['nickname'] = $this->faker->firstNameAscii();

        $checkHpArray = [
            '010-2851-6676', '01a141812', '121212312312312451234', '01f028516676', $param['hp']
        ];

        $errorMsg = str_replace('{KEY}', 'hp', $this->validateErrorMsg);

        foreach ($checkHpArray as $hp) {
            $param['hp'] = $hp;
            //var_dump($param);
            $this->returnRequestsetCallable($this, $param);

            $output = $this->request('POST', 'api/user');
            //var_dump($output);
            $this->assertResponseCode(400);

            $outputDecode = json_decode($output, true);

            $this->assertEquals($outputDecode[1], $errorMsg);
            unset($output);
        }
    }

    /**
     * @test 이메일
     *
     * 공백제거|필수|xss_clean|이메일|최대길이100|unique
     * @return void
     */
    public function test_user_email() : void
    {
        $param = $this->returnUserCreateParam();

        $this->userInsert($param);

        $param['nickname'] = $this->faker->firstNameAscii();
        $param['hp']       = str_replace('-', '', $this->faker->PhoneNumber);

        $checkEmailArray = [
            'asdas346gfdsdfg', 'fgdas56546', 'asfgagdgsd56', '436346346', $param['email']
        ];

        $errorMsg = str_replace('{KEY}', 'email', $this->validateErrorMsg);

        foreach ($checkEmailArray as $email) {
            $param['email'] = $email;
            //var_dump($param);
            $this->returnRequestsetCallable($this, $param);

            $output = $this->request('POST', 'api/user');
            //var_dump($output);
            $this->assertResponseCode(400);

            $outputDecode = json_decode($output, true);

            $this->assertEquals($outputDecode[1], $errorMsg);
            unset($output);
        }
    }

    /**
     * @test 성별
     *
     * 공백제거|xss_clean|포함[none,male,female]
     * @return void
     */
    public function test_user_sex() : void
    {
        $param = $this->returnUserCreateParam();
        $param['sex'] = 'asd';
        $errorMsg = str_replace('{KEY}', 'sex', $this->validateErrorMsg);
        //var_dump($param);
        $this->returnRequestsetCallable($this, $param);

        $output = $this->request('POST', 'api/user');
        //var_dump($output);
        $this->assertResponseCode(400);

        $outputDecode = json_decode($output, true);

        $this->assertEquals($outputDecode[1], $errorMsg);
        unset($output);
    }

    /**
     * @test 성별
     *
     * 공백제거|xss_clean|포함[none,male,female]
     * @return void
     */
    public function test_user_recommend_nickname() : void
    {
        $insertParam = $this->returnUserCreateParam();

        $this->userInsert($insertParam);

        $recommendNickname = $insertParam['nickname'];
        unset($insertParam);

        $recommendId = $this->CI->user_model->lastIndex();

        for ($i=0; $i<5; $i++) {
            $this->recommendInsert([
                'user_id' => rand(10, 999),
                're_user_id' => $recommendId
            ]);
        }

        $param = $this->returnUserCreateParam();

        $checkRecommendNickArray = [
            'asda314124', $recommendNickname
        ];

        $errorMsg = str_replace('{KEY}', 'recommend_nickname', $this->validateErrorMsg);

        foreach ($checkRecommendNickArray as $recommendNick) {
            $param['recommend_nickname'] = $recommendNick;
            //var_dump($param);
            $this->returnRequestsetCallable($this, $param);

            $output = $this->request('POST', 'api/user');
            //var_dump($output);
            $this->assertResponseCode(400);

            $outputDecode = json_decode($output, true);

            $this->assertEquals($outputDecode[1], $errorMsg);
            unset($output);
        }
    }

    /**
     * @test 등록
     *
     * @return void
     */
    public function test_user_create_success() : void
    {
        $insertParam = $this->returnUserCreateParam();

        $this->userInsert($insertParam);

        $recommendNickname = $insertParam['nickname'];
        unset($insertParam);

        $recommendId = $this->CI->user_model->lastIndex();

        $param = $this->returnUserCreateParam();
        $param['recommend_nickname'] = $recommendNickname;

        $this->returnRequestsetCallable($this, $param);

        $output = $this->request('POST', 'api/user');

        $this->assertResponseCode(200);

        $outputDecode = json_decode($output, true);

        $slug =  hash('sha1', $param['email'].$param['hp']);

        $this->assertEquals($outputDecode, [
            'slug' => $slug
        ]);
        unset($output);

        $userId = $this->CI->user_model->select('id', [
            ['where' => ['slug', $slug]],
            ['where' => ['delete_yn', 'N']]
        ])[0]['id'];

        $this->assertEquals(1, $this->CI->user_log_model->count([
            ['where' => ['user_id', $userId]],
            ['where' => ['action' , 'create']]
        ]));

        $this->assertEquals(1, $this->CI->user_recommend_model->count([
            ['where' => ['user_id', $userId]],
            ['where' => ['re_user_id' , $recommendId]]
        ]));
    }

    
    /**
     * 추천 등록
     *
     * @param array $param
     * @return void
     */
    public function recommendInsert(array $param) : void
    {
        $this->CI->user_recommend_model->insert($param);
    }
}
