<?php

/**
 * Class UserListTest
 */
class UserListTest extends TestCase
{
    /**
     * 유닛 테스트 실행시 자동 실행
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();

        $this->CI->load->model('v1/user_model', 'user_model');

        $this->CI->user_model->truncate();
    }

    /**
     * @test page & limit,
     *
     * 공백제거|xss_clean|정수
     *
     * 공백제거|xss_clean|정수
     *
     * @return void
     */
    public function test_user_list_page_limit()  : void
    {
        $queryStringArray = [
            ['?page=Asd&limit=SGDSG', 'page'],
            ['?page=2&limit=124sdfs', 'limit'],
            ['?page=-3&limit=-4', 'page'],
            ['?page=1&limit=-4', 'limit'],
            ['?page=-545', 'page'],
            ['?limit=-541', 'limit']
        ];

        foreach($queryStringArray as $subQueryStringArray) {
            //var_dump($subQueryStringArray);
            $output = $this->request('GET', 'api/user/'.$subQueryStringArray[0]);
            //var_dump($output);
            $this->assertResponseCode(400);

            $outputDecode = json_decode($output, true);

            $this->assertEquals(
                $outputDecode[1],
                str_replace('{KEY}', $subQueryStringArray[1], $this->validateErrorMsg)
            );
        }
    }

    /**
     * @test success,
     *
     * 공백제거|필수|xss_clean|최소길이2|최대길이20|custom
     *
     * @return void
     */
    public function test_user_list_success()  : void
    {
        $totalCount = 50;
        $this->userListInsert($totalCount);

        $equalArray = [
            'total_count' => $totalCount,
            'total_page'    => 0,
            'now_page'      => 2,
            'limit'         => 20,
            'list'          => []
        ];

        $equalArray['total_page'] = ceil($totalCount/$equalArray['limit']);

        $equalArray['list'] = $this->CI->user_model->userList([
            'page' => ($equalArray['now_page'] - 1) * $equalArray['limit'], 'limit' => $equalArray['limit']
        ]);
        //var_dump($equalArray);
        $output = $this->request('GET', 'api/user/?page='.$equalArray['now_page'].'&limit='.$equalArray['limit']);
        //var_dump($output);
        $this->assertResponseCode(200);

        $outputDecode = json_decode($output, true);
        //var_dump($equalArray);
        //var_dump($outputDecode);
        $this->assertEquals($outputDecode, $equalArray);

        $equalArray['now_page'] = 3;
        $equalArray['list'] = $this->CI->user_model->userList([
            'page' => ($equalArray['now_page'] - 1) * $equalArray['limit'], 'limit' => $equalArray['limit']
        ]);

        $this->assertEquals(10, sizeof($equalArray['list']));

        $output = $this->request('GET', 'api/user/?page='.$equalArray['now_page'].'&limit='.$equalArray['limit']);
        //var_dump($output);
        $this->assertResponseCode(200);

        $outputDecode = json_decode($output, true);
        //var_dump($equalArray);
        //var_dump($outputDecode);
        $this->assertEquals($outputDecode, $equalArray);
    }

    /**
     * 유저 리스트 등록
     * 
     * @param int $totalCount
     */
    public function userListInsert(int $totalCount = 50) : void
    {
        for ($i =0; $i < $totalCount; $i++) {
            $param = $this->returnUserCreateParam();
            $whereArray = [
                ['where' => ['nickname' , $param['nickname']]],
                ['or_where' => ['email' , $param['email']]],
                ['or_where' => ['hp', $param['hp']]]
            ];
            if ($this->CI->user_model->count($whereArray) > 0) {
                $i--;
                continue;
            }
            $this->userInsert($param);
        }
    }
}