<?php
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class My_Init
 *
 */
class My_Init
{
    /**
     * @var array
     */
    public $allowed_cors_origins = [
        '*'
    ];

    /**
     * @var array
     */
    public $allowed_cors_headers = [
        'Origin',
        'X-Requested-With',
        'Content-Type',
        'Accept',
        'Access-Control-Request-Method',
        'Authorization',
        'Access-Control-Allow-Method',
        'Access-Control-Allow-Origin',
    ];

    /**
     * @var array
     */
    public $allowed_cors_methods = [
         'GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'
    ];

    public function initExec()
    {
        header('Access-Control-Allow-Origin: '.implode(", ", $this->allowed_cors_origins));
        header('Access-Control-Allow-Headers: '.implode(", ", $this->allowed_cors_headers));
        header('Access-Control-Allow-Methods: '.implode(", ", $this->allowed_cors_methods));
    }
}
