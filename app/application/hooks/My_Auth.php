<?php
use Libraries\ResponseInfo;

defined('BASEPATH') or exit('No direct script access allowed');

/**
 * Class My_Auth
 *
 */
class My_Auth
{
	/**
	 * My_Auth constructor.
	 */
	public function __construct()
	{
	}

	/**
	 * 토큰 확인
	 *
	 * @param string $bearer
	 * @return bool|CI_Output
	 */
	public function checkAuthorizationKey(string $bearer = null)
	{
		$key = is_null($bearer) || empty($bearer) ? $this->getBearerKey() : $bearer;
		
		try {
		    if (empty($key)) {
		        throw new Exception();
            }
			$_SERVER['AUTH_CHECK'] = explode(" ", $key)[1] === API_KEY ? true : false;
        } catch (Exception $e) {
            $_SERVER['AUTH_CHECK'] = false;
        }

		return true;
	}

	/**
	 * 헤더 검색 후 리턴
	 * 
	 * @return mixed|string
	 */
	private function getBearerKey()
	{
		$bearer = '';
		$headerNameArray = ['HTTP_AUTHORIZATION'];
		foreach ($headerNameArray as $name) {
			if (isset($_SERVER[$name]) && !empty($_SERVER[$name])) {
				$bearer = $_SERVER[$name];
				break;
			}
		}
		return $bearer;
	}
}
