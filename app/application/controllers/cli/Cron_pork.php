<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require_once(APPPATH.'libraries/PorkTest.php');

class Cron_pork extends MY_Controller
{       
    /**
     * @var array $sendDateArray
     */
    private $sendDateArray = [
        '2019-12-10' => true,   
        '2019-12-20' => true   
    ];  

     /**
     * @var string $msg
     */
    private $msg = '{ID}님  쿠폰을 발행하였습니다.';

    /**
     * @var string $filePath
     */
    private $filePath = './application/logs';

    /**
     * @var array $fileNameArray
     */
    private $fileNameArray = [];
    
    /**
     * @var int $numChild 프로세스 카운트
     */
    private $numChild = 0;

    /**
     * @var string $interval_redis_key 
     */
    private $interval_redis_key = "month_coupon_send:usleep";

    /**  
     * 생성자
     */
    public function __construct()
    {
        echo "생성자 시작"."\r\n";
        
        parent::__construct();
    
        set_time_limit(0);
        
        ini_set('memory_limit','-1');

        if (!$this->input->is_cli_request()) {
            return;
        }
    
        $nowYmd = date('Y-m-d');
        if (ENVIRONMENT === 'production') {
            if (!isset($this->sendDateArray[$nowYmd])) {
                return;
            }   
        }

        $this->load->database();    
    
        $this->load->helper('file');

        $this->load->model('cli/month_send_coupon_sub');
    
        echo "생성자 종료"."\r\n";
    }

    /**
     * 크론 동작
     *  
     * @return void
     */
    public function index() 
    {       
        echo "인덱스 시작"."\r\n";
        $startDate = date('Y-m-d H:i:s');    
        $startTime = microtime(true);
        
        echo '쿠폰 발급 시작 시작일 : '.$startDate.', 시작타임 :'.$startTime."\r\n";

        $totalCount = 20000;
        echo '쿠폰 발급 대상 카운트 '.$totalCount."\r\n";

        $checkArray = [];
        $check = true;
        for($i=0; $i < $totalCount; $i++) {
            $checkArray[] = microtime().rand(1000, 9999);
            if (count($checkArray) > 999) {
                $fileName = base64_encode(rand(1000, 9999).microtime());
                
                if ($this->saveFile('send_'.$fileName, json_encode($checkArray), 'w+') === false) {
                    $check = false;
                    break;
                }
                $this->fileNameArray[] = $fileName;
                unset($checkArray);
            }
        }

        //$this->numChild = $this->getCpuCount();   
        $this->numChild = 8;
        for ($i = 0; $i < $this->numChild; $i++) {
            if (count($this->fileNameArray) === 0) {
                break;    
            }
          
            $this->jobRun(array_shift($this->fileNameArray));
        }
       
        while (pcntl_waitpid(0, $status) != -1) {
            if (count($this->fileNameArray) === 0) {
                continue;
            }           

            // job 할당
            $this->jobRun(array_shift($this->fileNameArray));
        }
        
        $endTime = microtime(true);
        $resultTime = $endTime - $startTime;

        echo '쿠폰 발급 종료  종료타임 : '.$endTime.', 경과타임: '.$resultTime."\r\n";
        exit;
    }

    /**
     * 작업
     * 
     * @param string $fileName
     * @param array $couponIds
     * @param array $couponCodeArray
     * @return void
     */
    private function jobRun($fileName) 
    {   
        $pid = pcntl_fork();

        if ($pid === -1) {
            echo '쿠폰 발급 종료. 프로세스 에러.'."\r\n";
            exit;
        }

        if ($pid === 0) {

            $this->db->close();
            $this->db->initialize(); 

            $fileFullPath = $this->filePath.'/send_'.$fileName;

            $jobObj = new PorkTest;

            $jobObj->ci = & get_instance();
            //$jobObj->useDb = & $this->db;
            //$jobObj->notiService = & $this->idus_notice_service;

            $jobObj->msg = $this->msg;
            $jobObj->nowDate = date('Y-m-d');
            echo $fileFullPath;
            $jobObj->readFileSetProperty($fileFullPath);
            
            $result = $jobObj();

            unlink($fileFullPath);
        
            //$this->childSleep();

            exit;
        }
        else {
            $this->db->close();
            $this->db->initialize(); 
        }
    }


    /**
     * 쿠폰 정보 가져오기
     * 
     * @return bool|array
     */
    private function getCouponInfo() 
    {           
        $result = $this->sdb->select('id, from_date, to_date, valid_period')->where_in('id', $this->configCouponIds)
            ->get('coupon');
        if ($result->num_rows === 0) {
            return false;
        }

        $returnArray = [];
        foreach ($result->result_array() as $coupon) {
            $returnArray[$coupon['id']]  = $coupon;
        }
        return $returnArray;
    }

    /**
     * cpu 카운트
     *      
     * @return int
     */
    private function getCpuCount() 
    {
        $pathCpuInfo = '/proc/cpuinfo';
        return !file_exists($pathCpuInfo) ? 1 : substr_count(file_get_contents($pathCpuInfo), 'processor');
    }
    
    /**
     * 쿠폰 발송 유저 카운트
     * 
     * @return int
     */
    private function getSendCounponUserCount() 
    {
        return (int) $this->sdb->select('count(user_id) as totalCount')->get($this->gradeTable)->result_array()[0]['totalCount'];
    }

    /**
     * 쿠폰 발송 유저 가져오기
     *  
     * @param int $user_id
     * @return bool|array
     */
    private function getSendCounponUsers($user_id)
    {
        $result =  $this->sdb->select('user_id, type_id, is_newbie')->where('user_id > ', $user_id)->order_by('user_id', 'ASC')->limit(1000)->get($this->gradeTable);
        if ($result->num_rows === 0) {
            return false;
        }
        
        return $result->result_array();
    }

    /**
     * 파일 저장
     *  
     * @param string $fileName
     * @param string $string
     * @param string $type
     * @return bool
     */
    private function saveFile($fileName, $string, $type)
    {
        if (!write_file($this->filePath.'/'.$fileName, $string, $type)) {
            return false;
        }
        return true;
    }

    /**
     * 노티 유저 등록
     * 
     * @param int $couponId
     * @return int
     */
    private function notiUserInsert($couponId)
    {
        $this->db->reconnect();
     
        $m_sec = round(microtime(true) * 1000);
        $item = array(
            "created" => $m_sec,
            "modified" => $m_sec,
            "noti_style" => 18,
            "artist_id" => 0,
            "artist_uuid" => '31be12c4-c498-11e3-8b03-06fd000000c2',
            "message" => $this->msg,
            "custom_uuid" => $couponId
        );
    
        $this->db->insert('noti_user', $item);
        return $this->db->insert_id();
    }

    /**
     * 프로세스 슬립
     * 
     * @return void
     */
    private function childSleep()
    {
        $this->load->model('redis_model');
        $redis = $this->redis_model->getClient();

        $interval = (int)$redis->get($this->interval_redis_key);
        if (!$interval) {
            //$interval = 300000; // 0.3s
            $interval = 3000000; //3s;
            $redis->setex($this->interval_redis_key, 172800, $interval);   // 2d
        }
        //$redis->close();
        unset($redis);
        //var_dump("sleep :".$interval);
        usleep($interval);    
    }
}