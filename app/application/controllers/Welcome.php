<?php
use Libraries\ResponseInfo;

defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends My_Controller
{
    /**
     *
     */
    public function index()
    {
        $response = ResponseInfo::$wrongUri;
        return return_response($response[0], $response);
    }
}
