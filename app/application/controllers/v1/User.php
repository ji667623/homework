<?php
use Libraries\ResponseInfo;

defined('BASEPATH') or exit('No direct script access allowed');


class User extends My_Controller
{
    /**
     * 생성자
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('v1/user_model', 'user_model');
        $this->load->model('v1/user_recommend_model', 'user_recommend_model');
        $this->load->model('v1/user_log_model', 'user_log_model');

        $this->load->helper(['form']);
    }

    /**
     * 유저 생성
     */
    public function create()
    {
        $checkArray = $this->validateReturn();

        if ($checkArray['success']  === false) {
            return return_response($checkArray['data'][0], $checkArray['data']);
        }

        $param = $checkArray['data'];
        unset($checkArray);

        $recommendNickName = '';
        if (isset($param['recommend_nickname']) && !empty($param['recommend_nickname'])) {
            $recommendNickName = $param['recommend_nickname'];
        }

        $param['slug'] = hash('sha1', $param['email'].$param['hp']);
        $param['password'] = password_hash($param['password'], PASSWORD_DEFAULT);
        $param['insert_ip'] = $this->input->ip_address();

        unset($param['password_confirm'], $param['recommend_nickname']);

        if ($this->user_model->userCreate($param, $recommendNickName) === false) {
            return return_response(
                ResponseInfo::$serverError[0],
                ResponseInfo::$serverError
            );
        }

        return return_response(200, ['slug' => $param['slug']]);
    }

    /**
     *  유저 리스트
     */
    public function list()
    {
        $page =  is_null($this->input->get('page')) ? 1 : $this->input->get('page');
        $limit =  is_null($this->input->get('limit')) ? 10 : $this->input->get('limit');

        $param = [
            'page'  => $page, 'limit' => $limit
        ];

        $checkArray = $this->validateReturn($param);
        if ($checkArray['success']  === false) {
            return return_response($checkArray['data'][0], $checkArray['data']);
        }

        $returnArray = [
            'total_count'   => 0,
            'total_page'    => 0,
            'now_page'      => (int) $page,
            'limit'         => (int) $limit,
            'list'          => []
        ];

        if (($returnArray['total_count'] = $this->user_model->count()) === 0) {
            return return_response(
                200,
                $returnArray
            );
        }

        $returnArray['total_page'] = ceil($returnArray['total_count']/$returnArray['limit']);
        if ($returnArray['now_page'] > $returnArray['total_page']) {
            return return_response(
                200,
                $returnArray
            );
        }

        $returnArray['list'] = $this->user_model->userList([
            'page'  => ($returnArray['now_page'] - 1) * $returnArray['limit'],
            'limit' => $returnArray['limit']
        ]);

        return return_response(
            200,
            $returnArray
        );
    }

    /**
     * 유저 상세
     *
     * @param string $slug
     */
    public function info(string $slug)
    {
        $param = ['slug' => $slug];

        $checkArray = $this->validateReturn($param);
        if ($checkArray['success']  === false) {
            return return_response($checkArray['data'][0], $checkArray['data']);
        }
        unset($checkArray);

        $userInfo = $this->user_model->select('slug, name, nickname, hp, email', [
            ['where' => ['slug', $slug]]
        ])[0];

        return return_response(200, $userInfo);
    }

    /**
     * 유저 삭제
     *
     * @param string $slug
     */
    public function delete(string $slug)
    {
        $param = array_merge([
            'slug'=> $slug
        ], $this->getJsonBodyToArray());

        $checkArray = $this->validateReturn($param);
        if ($checkArray['success']  === false) {
            return return_response($checkArray['data'][0], $checkArray['data']);
        }
        unset($checkArray);

        if ($this->form_validation->checkPassword($param) === false) {
            $response =  ResponseInfo::$validate;
            $response[1] = str_replace('{KEY}', 'password', $response[1]);
            return return_response($response[0], $response);
        }

        $param['insert_ip'] = $this->input->ip_address();

        if ($this->user_model->userDelete($param) === false) {
            return return_response(
                ResponseInfo::$serverError[0],
                ResponseInfo::$serverError
            );
        }

        return return_response(200, ['slug' => $param['slug']]);
    }

    /**
     * 유저 수정
     *
     * @param string $slug
     */
    public function update(string $slug)
    {
        $param = array_merge([
            'slug'=> $slug
        ], $this->getJsonBodyToArray());

        $checkArray = $this->validateReturn($param);
        if ($checkArray['success']  === false) {
            return return_response($checkArray['data'][0], $checkArray['data']);
        }
        unset($checkArray);

        $checkArray = $this->form_validation->checkUpdateUnique($param);
        if ($checkArray['success'] === false) {
            $response =  ResponseInfo::$validate;
            $response[1] = str_replace('{KEY}', $checkArray['field'], $response[1]);
            return return_response($response[0], $response);
        }
        unset($checkArray);

        $checkSlug = $param['slug'];
        $param['slug'] = hash('sha1', $param['email'].$param['hp']);
        $param['insert_ip'] = $this->input->ip_address();

        if ($this->user_model->userUpdate($checkSlug, $param) === false) {
            return return_response(
                ResponseInfo::$serverError[0],
                ResponseInfo::$serverError
            );
        }
        unset($param['insert_ip']);

        return return_response(200, $param);
    }
}
