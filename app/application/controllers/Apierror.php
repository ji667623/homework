<?php
use Libraries\ResponseInfo;

defined('BASEPATH') or exit('No direct script access allowed');

class Apierror extends My_Controller
{
    /**
     * 생성자
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * 에러 관련 라우팅 처리
     *
     * @throws ReflectionException
     */
    public function error_404()
    {
        return $this->returnErrorResponse('wrongUri');
    }

    /**
     * 리스폰스 코드와 메시지를 전달
     * 
     * @param string $errorPropertyName
     * @throws ReflectionException
     */
    public function returnErrorResponse(string $errorPropertyName)
    {
        $checkResponsArray  = ResponseInfo::$serverError;
        if ($this->reflect($errorPropertyName) === true) {
            $checkResponsArray = ResponseInfo::${$errorPropertyName};
        }
        return return_response($checkResponsArray[0], $checkResponsArray);
    }
}
