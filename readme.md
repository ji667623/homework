# Codeigniter를 이용한 Restful API 개발

## 내용

CodeIgniter Phpunit  [링크]: (https://github.com/kenjis/ci-phpunit-test)

데이테베이스 개발 환경에 따라 물리적 분기

    * app/application/config/development/database.php (homework)
    * app/application/config/testing/config/database.php (homework_test)
    * 유닛 테스트 실행시 testing 개발 환경을 읽어들입니다.

UnitTest 

    * Codeigniter 유닛테스트 시 글로벌 하게 설치한 Phpunit을 쓰게 돼면 버전 문제로 인해 정상적인 테스팅이 어려워 집니다.
    * 실행 
        * app/application/tests 폴더 내부의 phpunit 파일로 테스트 실행
        * ./phpunit -- filter 메소드명  (특정 메소드만 실행)
            * ex) ./phpunit --filter test_user_create_success
        * ./phpunit 파일경로 (특정 파일만 실행)
            * ex) ./phpunit contorllers/UserDeleteTest 
        * ./phpunit (모든 테스트 실행)
            * ex) ./phpunit 
    * controllers 와 hooks 쪽 테스트 케이스 작성 돼어 있습니다.
 ![ex_phpunit] (./image/phpunit.png) 
 
 인증 
 
    * 모든 API request 전송시 header Authorization 에 app/config/constants.php 에 정의 돼어 있는 API_KEY 를 넣었고 bearer 토큰 형식으로 던지게 돼면  hooking pre_controller 단에서 검증 하게 됍니다.

벨리데이션
 
    * app/applicatyion/config/form_validation.php 
    * app/applicatyion/config/validation/v1/user/*

API
 
    * request 값들은 GET 은 quqery string 형태 
    * GET을 제외한 것은 json body 형태 
    * response 는 http 상태 값 및 결과 또는 예외 메시지 전달

Header

    * Content-Type:application/json
    * Authorization:Bearer @Ifsmer946l0gmsg49!kfdfgm4
   


GET api/user 

    * 유저 리스트 
        * reqpeust 예제 
            * api/user/?page=1&limit=10
                * page = trim|xss_clean|integer|is_natural_no_zero
                * limit = trim|xss_clean|integer|is_natural_no_zero
        * response 예제
        {
            "total_count": 50,
            "total_page": 5,
            "now_page": "3",
            "limit": "10",
            "list": [
                {
                    "slug": "02d93f673b5568db696fd7acbfc9e26b70a00282",
                    "nickname": "wonjun",
                    "NAME": "유영호",
                    "hp": "04138654788",
                    "email": "ha.wonjun@chung.biz"
                },
                {
                    "slug": "4619e3655b4b78fedaa40e37b73b58fdd6dad628",
                    "nickname": "seulki",
                    "NAME": "정윤영",
                    "hp": "15467108",
                    "email": "youngjin.cheon@son.biz"
                },
                {
                    "slug": "3db4340ecb7c8bed7527491ec1936c3216c1d5e5",
                    "nickname": "hyunjun",
                    "NAME": "조용태",
                    "hp": "18343868",
                    "email": "kyunghwan.baek@ryu.org"
                },
                {
                    "slug": "49a6c1782316cea068c510aca3b9d9b13cc6832c",
                    "nickname": "taeho",
                    "NAME": "류선영",
                    "hp": "05022266187",
                    "email": "jiwoo90@yahoo.com"
                },
                {
                    "slug": "dced55045a4cb02302877b40dbc1596ba06f7e72",
                    "nickname": "woojin",
                    "NAME": "홍혜나",
                    "hp": "18317244",
                    "email": "imoon@gwon.info"
                },
                {
                    "slug": "3473033646453d40b2c7af9b7d361efe80af7a84",
                    "nickname": "sieun",
                    "NAME": "윤민철",
                    "hp": "01019682815",
                    "email": "oyang@cheon.kr"
                },
                {
                    "slug": "e0bcaffe956eafc9e4028e9977c652ac8774eb7b",
                    "nickname": "sangah",
                    "NAME": "유현정",
                    "hp": "15303012",
                    "email": "nam.seojun@son.kr"
                },
                {
                    "slug": "c030b3357fa8824ead0937378a042206fe8b2a2b",
                    "nickname": "geongeun",
                    "NAME": "조기수",
                    "hp": "04151246668",
                    "email": "cho.eunhye@hotmail.com"
                },
                {
                    "slug": "389185a0f56512b8b6efe887522ac37baeeb2c7b",
                    "nickname": "seoyoung",
                    "NAME": "기수원",
                    "hp": "06925002354",
                    "email": "jinwoo.bae@gmail.com"
                },
                {
                    "slug": "40684c548f81e9791f3e6edd14cae1db1ded505a",
                    "nickname": "haeun",
                    "NAME": "석정란",
                    "hp": "18569844",
                    "email": "jo.jihoo@gmail.com"
                }
            ]
        }
![get_api_user_image] (./image/get_api_user.png)

GET api/user/info/(:slug)

    * 유저 상세
        * request 예제 
            * api/user/info/38c399d3f3ffac8eb0314b2e4818c576fc8d2d7c
                * slug =  trim|required|xss_clean|유저 테이블에 등록 돼어 있고 삭제 안돼있는지 확인
        * response 예제
        {
            "slug": "02d93f673b5568db696fd7acbfc9e26b70a00282",
            "name": "유영호",
            "nickname": "wonjun",
            "hp": "04138654788",
            "email": "ha.wonjun@chung.biz"
        }
![get_api_user_info_auth_image] (./image/get_api_user_info_slug.png)
            
POST api/user 

    * 유저 등록
        * request 예제
            * api/user 
                * nane = 공백제거|필수|xss_clean|최소길이2|최대길이20|한글또는영문대소문자
                * nickname = 공백제거|필수|xss_clean|최소길이1|최대길이30|영문소문자만허용|영문|유니크
                * password = 공백제거|필수|xss_clean|최소길이10|최대길이255|영문대문자, 영문 소문자, 특수 문자, 숫자 각 1개 이상씩 포함
                * password_confirm = 필수|password매칭
                * hp = 공백제거|필수|xss_clean|최대길이20|숫자|유니크
                * email = 공백제거|필수|xss_clean|이메일|최대길이100|유니크
                * sex = 공백제거|xss_clean|포함[none,male,female]
                * recommand_nickname = 공백제거|xss_clean|등록돼어있고삭제안된유저의닉네임|추천횟수5회미만
                * json body 
                {
                    "name"		: "조재성",
                    "nickname"	: "jinybh",
                    "password"  : "!!@@1234Nqnc",
                    "password_confirm" : "!!@@1234Nqnc",
                    "hp" : "01028516576",
                    "email" : "ji666@gmail.com",
                    "sex" : "male",
                    "recommend_nickname" : "jihoo"
                 }
        * response 예제 
        {
            "slug": "abca18b1d60591eac77f04c6e73c06ad1c8acea3"
        }
![post_api_user_image] (./image/post_api_user.png)

PUT api/user/(:slug)
    
    * 유저 수정 
        * request 예제
            * api/user/38c399d3f3ffac8eb0314b2e4818c576fc8d2d7c
                * slug =  trim|required|xss_clean|유저 테이블에 등록 돼어 있고 삭제 안돼있는지 확인
                * nane = 공백제거|필수|xss_clean|최소길이2|최대길이20|한글또는영문대소문자
                * nickname = 공백제거|필수|xss_clean|최소길이1|최대길이30|영문소문자만허용|영문|자기제외유니크
                * hp = 공백제거|필수|xss_clean|최대길이20|숫자|자기제외유니크
                * email = 공백제거|필수|xss_clean|이메일|최대길이100|자기제외유니크
                * sex = 공백제거|xss_clean|포함[none,male,female]
                * json body
                {
                	"name"		: "조재성흐",
                	"nickname"	: "jinybhasd",
                	"hp" : "01028526576",
                	"email" : "ji626@gmail.com",
                	"sex" : "male"
                }                          
        * response 예제 
        {
            "slug": "5eab38da88473863ca0a9a6632698e3c8b3ef218",
            "name": "조재성흐",
            "nickname": "jinybhasd",
            "hp": "01028526576",
            "email": "ji626@gmail.com",
            "sex": "male"
        }
![put_api_user_slug] (./image/put_api_user_slug.png)
       
DELETE api/user/(:slug)

    * 유저 삭제
        * request 예제
            * api/user/38c399d3f3ffac8eb0314b2e4818c576fc8d2d7c
                * slug =  trim|required|xss_clean|유저 테이블에 등록 돼어 있고 삭제 안돼있는지 확인
                * password = 공백제거|필수|xss_clean|최소길이10|최대길이255|영문대문자, 영문 소문자, 특수 문자, 숫자 각 1개 이상씩 포함
                * json body
                {
                	"password" : "!!@@1234Nqnc"
                }
        * response 예제 
        {
            "slug": "5eab38da88473863ca0a9a6632698e3c8b3ef218"
        }
![delete_api_user_slug] (./image/delete_api_user_slug.png)
        